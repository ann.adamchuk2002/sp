#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

const size_t BUFF_SIZE = 1024;

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Missing label, exit");
        exit(1);
    } 
    char *label = argv[1];
    fd_set rfds;
    struct timeval tv;
    int retval, len;
    char buff[BUFF_SIZE];

    while(1) {
        FD_ZERO(&rfds);
        FD_SET(STDIN_FILENO, &rfds);
        tv.tv_sec = 5;
        tv.tv_usec = 0;
        
        retval = select(STDIN_FILENO + 1, &rfds, NULL, NULL, &tv);
        if (retval == -1)
        {
            printf("Error: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        else if (retval)
        {
            if (FD_ISSET(STDIN_FILENO, &rfds)) {
                ssize_t readBytes = read(STDIN_FILENO, buff, BUFF_SIZE);
                len = readBytes - 1;
                if (buff[len] == '\n')
                {
                    buff[len] = '\0';
                }
                printf("%s: '%s'\n", label, buff);
            }
        }
        else
        {
            printf("%s: timeout.\n", label);
        }
    }
    return 0;
}