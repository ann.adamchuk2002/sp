#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

const size_t BUFFER_SIZE = 512;

ssize_t processBuffer(char *buffer, ssize_t size)
{
    ssize_t changedBytes = 0;
    for (ssize_t i = 0; i <= size; i++)
    {
        if (buffer[i] >= 'a' && buffer[i] <= 'z')
        {
            buffer[i] -= 32;
            changedBytes++;
        }
    }
    return changedBytes;
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Missing filenames, exit");
        exit(1);
    }
    char *input = argv[1];
    char *output = argv[2];
    int fIn = open(input, O_RDONLY);
    if (fIn == -1)
    {
        printf("Unable to open input file %s.\n Error: %s\n", input, strerror(errno));
        exit(1);
    }
    int fOut = open(output, O_CREAT | O_WRONLY | O_APPEND, 0644);
    if (fOut == -1)
    {
        printf("Unable to open/create output file %s.\n Error: %s\n", output, strerror(errno));
        exit(1);
    }

    char buffer[BUFFER_SIZE + 1];
    ssize_t readBytes = 0;
    ssize_t totalBytes = 0;
    ssize_t changedBytes = 0;
    do
    {
        readBytes = read(fIn, buffer, BUFFER_SIZE);
        totalBytes += readBytes;
        changedBytes += processBuffer(buffer, BUFFER_SIZE);
        write(fOut, buffer, BUFFER_SIZE);
    } while (readBytes == BUFFER_SIZE);
    printf("Read %li bytes\n", totalBytes);
    printf("Changed %li bytes\n", changedBytes);
    close(fIn);
    close(fOut);
    return 0;
}