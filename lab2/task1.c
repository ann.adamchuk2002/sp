#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
    pid_t child = fork();
    if (child == 0) {
        printf("Child process:\n");
        printf("Pid: %d\n", getpid());
        printf("Ppid: %d\n", getppid());
        printf("Gid: %d\n", getgid());
        printf("Egid: %d\n", getegid());
        printf("Sid: %d\n", getsid(0));
        printf("Uid: %d\n", getuid());
        printf("Euid: %d\n", geteuid());
        printf("Child process ended\n");
    } else {
        printf("Parent process:\n");
        printf("Pid: %d\n", getpid());
        printf("Ppid: %d\n", getppid());
        printf("Gid: %d\n", getgid());
        printf("Egid: %d\n", getegid());
        printf("Sid: %d\n", getsid(0));
        printf("Uid: %d\n", getuid());
        printf("Euid: %d\n", geteuid());
        printf("Child process pid: %i\n", child);
        wait(NULL);
        printf("Parent process ended\n");
    }
    return 0;
}