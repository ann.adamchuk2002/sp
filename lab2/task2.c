#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char* argv[])
{
    FILE *logFile= fopen("./log.txt", "w");
    fprintf(logFile, "program started %s %s\n",__DATE__, __TIME__);
    fflush(logFile);
    pid_t childPid = fork();

    if (childPid < 0)
    {
        fprintf(stderr, "Error while forking\n");
        exit(1);
    }

    if (childPid > 0)
    {
        fprintf(logFile, "create child with PID %i\n", childPid);
        exit(0);
    }

    pid_t sid = setsid();
    if(sid < 0)
    {
        fprintf(stderr, "Error while set sid\n");
        exit(1);
    }
    chdir("/");
    int size = getdtablesize();
    for (int i = 0; i < size; i++)
    {
        close(i);
    }

    open("/dev/null", O_RDONLY, stdin);
    open("/dev/null", O_WRONLY, stdout);
    open("/dev/null", O_WRONLY, stderr);

    FILE * newLogFile= fopen("log2.txt", "a+t");

    fprintf(newLogFile, "Pid: %d\n", getpid());
    fprintf(newLogFile, "Ppid: %d\n", getppid());
    fprintf(newLogFile, "Gid: %d\n", getgid());
    fprintf(newLogFile, "Egid: %d\n", getegid());
    fprintf(newLogFile, "Sid: %d\n", getsid(0));
    fprintf(newLogFile, "Uid: %d\n", getuid());
    fprintf(newLogFile, "Euid: %d\n", geteuid());
    while (1)
    {
        sleep(1);
        fprintf(newLogFile, "Logging info...\n");
        fflush(newLogFile);
    }
    fflush(newLogFile);
    fclose(newLogFile);
}