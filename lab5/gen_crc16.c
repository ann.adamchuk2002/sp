#include <stdio.h>
#include "./gen_crc16.h"

uint16_t gen_crc16(const uint8_t *data, uint16_t size)
{
    uint16_t out = 0;
    int bitsRead = 0, bitFlag;

    if(data == NULL)
        return 0;

    while(size > 0)
    {
        bitFlag = out >> 15;

        out <<= 1;
        out |= (*data >> bitsRead) & 1;

        bitsRead++;
        if(bitsRead > 7)
        {
            bitsRead = 0;
            data++;
            size--;
        }

        if(bitFlag)
            out ^= 0x8005;

    }
    int i;
    for (i = 0; i < 16; ++i) {
        bitFlag = out >> 15;
        out <<= 1;
        if(bitFlag)
            out ^= 0x8005;
    }

    uint16_t crc = 0;
    i = 0x8000;
    int j = 0x0001;
    for (; i != 0; i >>=1, j <<= 1) {
        if (i & out) crc |= j;
    }

    return crc;
}
